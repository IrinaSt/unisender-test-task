const getFlightsFilterList = () => { // tabs for filtering flights
    const filterList = [
        '<li data-target="all">All</li>',
    ];
    flights.flights.forEach(flight => {
        filterList.push(
            `<li data-target="${flight.carrier}">${flight.carrier}</li>`
        )
    });

    return Array.from(new Set(filterList)).join('');
}

const getflightsContent = (filter) => { // get content when click on filter tab
    const filteredContent = flights.flights.filter(flight => {
        if (filter === "all") {
            return flight;
        } else {
            return flight.carrier === filter;
        }
    });

    return filteredContent.map(flight => {
        let carrierImage = '';
        const carrierImageInvailableData = flightsCompaniesLogos.filter(element => {
            return flight.carrier == element.carrier;
        })[0];
        if (carrierImageInvailableData) {
            carrierImage = carrierImageInvailableData.logo;
        } else {
            carrierImage = 'img/default-company.jpg';
        }


        const { carrier, direction, arrival, departure } = flight;

        return `<a href="#" class="flight-card">
             <div class="flight-company-logo">
              <img src="${carrierImage}" alt="">
              <h6 class="flight-carrier">${carrier}</h6>
             </div>
             <div class="flight-info">
              <p>From <span class="flight-direction">${direction.from}</span> at ${arrival} </p>
              <p>To <span class="flight-direction">${direction.to}</span> at ${departure}</p>
             </div>
            </a>`
    }).join('');
};

document.querySelector('.flights-carriers').innerHTML = getFlightsFilterList();
document.getElementById("flights").innerHTML = getflightsContent("all");

document.addEventListener('click', function(event) {
    if (event.target.dataset.target) {
        document.getElementById("flights").innerHTML = getflightsContent(event.target.dataset.target);
    };
});

document.getElementById('burgerMenu').addEventListener('click', function() {
    document.querySelector('.header-menu').classList.toggle('show');
})